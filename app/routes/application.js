// import Route from '@ember/routing/route';

// export default class ApplicationRoute extends Route {
// }
import Route from '@ember/routing/route';
export default Route.extend({
  model() {
    return this.store.findAll('employee');
  },
  actions: {
    deleteEm(employee) {
      let confirmation = confirm('Are you sure?');

      if (confirmation) {
        employee.destroyRecord();
      }
    },
    // addEm(employee) {
    //   let post = this.store.createRecord('employee', {
    //     nim: 'Rails is Omakase',
    //     nama: 'Lorem ipsum',
    //   });
    //   post.save();
    // },
  },
});
