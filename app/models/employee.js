// import Model from '@ember-data/model';

// export default class EmployeeModel extends Model {

// }
import DS from 'ember-data';
export default DS.Model.extend({
  nim: DS.attr('string'),
  nama: DS.attr('string'),
});
