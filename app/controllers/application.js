// import Controller from '@ember/controller';

// export default class ApplicationController extends Controller {
// }
import Controller from '@ember/controller';

export default Controller.extend({
  actions: {
    createEmployee() {
      let newEmployeeNim = this.get('newEmployeeNim');
      let newEmployeeName = this.get('newEmployeeName');

      let newRecord = this.store.createRecord('employee', {
        nim: newEmployeeNim,
        nama: newEmployeeName,
      });

      newRecord.save();
    },
    readData(readed) {
      let id = readed;
      this.store.findRecord('employee', id).then((game) => {
        alert(game.get('nim') + ' ' + game.get('nama'));
      });
    },
    updateEmployee(updated, nimd, namad) {
      let updatedNim = this.get('updatedEmployee');
      let data = this.get('model').findBy('id', updated);
      data.save('employee', { nim: nimd, nama: namad });
    },
    destroyEmployee(destroy) {
      let destroyId = destroy;
      let data = this.get('model').findBy('id', destroyId);
      data.destroyRecord();
      window.location.reload(true);
    },
  },
});
