// import JSONAPISerializer from '@ember-data/serializer/json-api';

// export default class EmployeeSerializer extends JSONAPISerializer {}
import DS from 'ember-data';

export default DS.JSONSerializer.extend({
  extractErrors(store, typeClass, payload, id) {
    if (payload && typeof payload === 'object' && payload._problems) {
      payload = payload._problems;
      this.normalizeErrors(typeClass, payload);
    }
    return payload;
  },
});
